import { APIResponse } from "@playwright/test";
import { COMMENTS } from "../routes/routes";
import { APIBase } from "./apiBase";

export class CommentAPI extends APIBase {
    async createComment(articleSlug: string, comment: string): Promise<APIResponse> {
        const payLoad = {
            "comment": {
              "body": comment
            }
          }
        return this.executePostRequest(`${this.API_BASE_URL}${COMMENTS.replace(':slug', articleSlug)}`, payLoad);
    }

    async commentPOST(articleSlug: string, payLoad: {}) : Promise<APIResponse> {
      return this.executePostRequest(`${this.API_BASE_URL}${COMMENTS.replace(':slug', articleSlug)}`, payLoad);
    }

    async commentListGET(articleSlug: string) : Promise<APIResponse> {
      return this.executeGetRequest(`${this.API_BASE_URL}${COMMENTS.replace(':slug', articleSlug)}`);
    }
};