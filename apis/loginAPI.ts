import { LOGIN } from "../routes/routes";
import { APIBase } from "./apiBase";

export type User = {
    user: userData
}

type userData = {
    username: string,
    email: string,
    token: string
}

export class LoginAPI extends APIBase {

    async login(email: string, password: string): Promise<User> {
        const payLoad = {
            "user": {
                "email": `${email}`,
                "password": `${password}`
            }
        }
        return await (await (this.executePostRequest(`${this.API_BASE_URL}${LOGIN}`, payLoad))).json();
    }
};