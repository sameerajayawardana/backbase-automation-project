import { APIResponse, request } from "@playwright/test";

export class APIBase {
    readonly API_BASE_URL = (process.env.TEST_ENVIRONMENT == "prod" ? process.env.PROD_API_URL : process.env.QA_API_URL) || "https://qa-task.backbasecloud.com/api";
    readonly BASIC_AUTH_TOKEN = (process.env.TEST_ENVIRONMENT == "prod" ? process.env.PROD_BASIC_AUTH : process.env.QA_BASIC_AUTH) || "Y2FuZGlkYXRleDpxYS1pcy1jb29s";
    private HEADERS = {
        "Content-type": "application/json",
        "Authorization": `BASIC ${this.BASIC_AUTH_TOKEN}`,
        "jwtauthorization": ""
    };

    async setHeaderJWT(value: string): Promise<void> {
        this.HEADERS.jwtauthorization = `Token ${value}`;
    }

    async getHeaders() {
        return this.HEADERS;
    }

    async executePostRequest(url: string, data: {}): Promise<APIResponse> {
        return (await request.newContext()).post(url, {
            headers: await this.getHeaders(),
            data
        }
        );
    }

    async executeGetRequest(url: string): Promise<APIResponse> {
        return (await request.newContext()).get(url, {
            headers: await this.getHeaders()
        }
        );
    }
}