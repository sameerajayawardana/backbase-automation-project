import { APIResponse } from "@playwright/test";
import { ARTICLES } from "../routes/routes";
import { APIBase } from "./apiBase";

export type ArticleInfo = {
  slug: string,
  title: string,
  description: string,
  body: string,
  tagList: string[],
};

export class ArticleAPI extends APIBase {
  async createArticle(title: string, about: string, body: string, tags?: string[]): Promise<APIResponse> {
    const payLoad = {
      "article": {
        "title": title,
        "description": about,
        "body": body,
        "tagList": tags
      }
    }
    return this.executePostRequest(`${this.API_BASE_URL}${ARTICLES}`, payLoad);
  }

  async articlePOST(payLoad): Promise<APIResponse> {
    return this.executePostRequest(`${this.API_BASE_URL}${ARTICLES}`, payLoad);
  }

  async articleListGET(): Promise<APIResponse> {
    return this.executeGetRequest(`${this.API_BASE_URL}${ARTICLES}`);
  }

  async articleGET(slug: string): Promise<APIResponse> {
    return this.executeGetRequest(`${this.API_BASE_URL}${ARTICLES}/${slug}`);
  }
};