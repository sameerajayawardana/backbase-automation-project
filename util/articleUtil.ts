import { ElementHandle, Page } from "@playwright/test";
import * as _ from 'lodash';
import { delay, log } from "./logUtil";

export type Article = {
    author: string,
    title: string,
    viewLink?: ElementHandle<HTMLElement | SVGElement>
};


export const extractAllArticles = async (page: Page, elementSelector: string): Promise<Article[]> => {
    const articles: Article[] = [];
    
    const elements = await page.$$(elementSelector);

    for (const element of elements) {
        articles.push({
            author: await (await element.$('a[class="author"]')).textContent(),
            title: (await (await element.$('a[class="preview-link"] h1')).textContent()),
            viewLink: await element.$('a[class="preview-link"] span')
        })
    };
    return articles;
};

export const searchArticleByTitle = async (articles: Article[], searchtext: string) : Promise<Article[]> => {
    log(`Searching articles containing '${searchtext}'`);

    return articles.filter(a => a.title.includes(searchtext));
}