export let browser = '';

export const setBrowserForLogs = (browserName) : void => {
   browser = browserName;
}

export const log = (content: string) : void => {
   console.info(browser,'==> INFO: ',content);   
};

export const delay = async (time: number) : Promise<void> => {
   return new Promise(function(resolve) { 
      setTimeout(resolve, time)
  });
};