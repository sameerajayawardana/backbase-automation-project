import { Page, test as baseTest } from '@playwright/test'
import { LoginPage } from '../pageObjects/loginPage'
import { HomePage } from '../pageObjects/homePage'
import { ArticlesPage } from '../pageObjects/articlesPage'

/**
 * This extends base test to implement page fixtures. Fixtures help to
 * maintain a well organized page object model
*/
const test = baseTest.extend<{
  newPage: Page,
  loginPage: LoginPage,
  homePage: HomePage,
  articlesPage: ArticlesPage,
}>({
  newPage: async ({browser}, use) => {
    const context = await browser.newContext({
      httpCredentials: {
        username: "candidatex",
        password: "qa-is-cool"
      }
    });
    await use(await context.newPage()); 
  },
  loginPage: async ({ newPage }, use) => {
    
    await use(new LoginPage(newPage))
  },
  homePage: async ({ newPage }, use) => {
    await use(new HomePage(newPage))
  },
  articlesPage: async ({ newPage }, use) => {
    await use(new ArticlesPage(newPage))
  }
})

export default test
export const expect = test.expect

