import { test as baseTest } from '@playwright/test'
import { LoginAPI } from '../apis/loginAPI'
import { ArticleAPI } from '../apis/articleAPI'
import { CommentAPI } from '../apis/commentAPI'

/**
 * This extends base test to implement API fixtures. Fixtures help to
 * maintain a well organized structure for tests
*/
const test = baseTest.extend<{
  loginAPI: LoginAPI,
  articleAPI: ArticleAPI,
  commentAPI: CommentAPI,
}>({
  loginAPI: async ({ page }, use) => {
    await use(new LoginAPI())
  },
  articleAPI: async ({ page }, use) => {
    await use(new ArticleAPI())
  },
  commentAPI: async ({ page }, use) => {
    await use(new CommentAPI())
  },
})

export default test
export const expect = test.expect

