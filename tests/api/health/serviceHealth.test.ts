import { user1 } from '../../../data/user';
import test, { expect } from '../../../fixtures/apis';

test('HEALTH_1 - User service health', async ({ loginAPI }) => {
    const res = await loginAPI.login(user1.email, user1.pass);
    expect(res).not.toBeNull();
});

test('HEALTH_2 - Article service health', async ({ articleAPI }) => {
    const res = await articleAPI.articleListGET();
    expect(res.status()).toEqual(200);
});