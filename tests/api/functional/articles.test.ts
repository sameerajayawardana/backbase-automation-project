import * as faker from 'faker';
import { ArticleInfo } from '../../../apis/articleAPI';
import { user1 } from '../../../data/user';
import test, { expect } from '../../../fixtures/apis';

test('AF3 - Creating ariticle without tags', async ({ loginAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const res = await articleAPI.createArticle(title, about, content);
    const resArticle = (await res.json()).article as ArticleInfo
    expect(res.status()).toEqual(200);
    expect(resArticle.title).toEqual(title);
    expect(resArticle.description).toEqual(about);
    expect(resArticle.body).toEqual(content);
    expect(resArticle.tagList).toEqual([]);
});

// Failed due to BUG-5 - Refer the test report, Bugs section
test('AF4 - Create article without title', async ({ loginAPI, articleAPI }) => {
    const payLoad = {
        "article": {
            "description": await faker.lorem.sentence(),
            "body": await faker.lorem.paragraph(),
            "tagList": await faker.lorem.word()
        }
    }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const res = await articleAPI.articlePOST(payLoad);
    const resArticle = (await res.json()).article as ArticleInfo
    expect(res.status()).toEqual(500);
});

// Failed due to BUG-6 - Refer the test report, Bugs section
test('AF5 - Create article without description', async ({ loginAPI, articleAPI }) => {
    const payLoad = {
        "article": {
            "title": await faker.lorem.sentence(),
            "body": await faker.lorem.paragraph(),
            "tagList": await faker.lorem.word()
        }
    }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const res = await articleAPI.articlePOST(payLoad);
    const resArticle = (await res.json()).article as ArticleInfo
    expect(res.status()).toEqual(500);
});

// Failed due to BUG-6 - Refer the test report, Bugs section
test('AF6 - Create article without body', async ({ loginAPI, articleAPI }) => {
    const payLoad = {
        "article": {
            "title": await faker.lorem.sentence(),
            "description": await faker.lorem.sentence(),
            "tagList": await faker.lorem.word()
        }
    }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const res = await articleAPI.articlePOST(payLoad);
    const resArticle = (await res.json()).article as ArticleInfo
    expect(res.status()).toEqual(500);
});

// Failed due to BUG-7 - Refer the test report, Bugs section
test('AF7 - Create article with empty required fields', async ({ loginAPI, articleAPI }) => {
    const payLoad = {
        "article": {
            "title": "",
            "description": "",
            "body": ""
        }
    }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const res = await articleAPI.articlePOST(payLoad);
    const resArticle = (await res.json()).article as ArticleInfo
    expect(res.status()).toEqual(500);
});

test('AF8 - Create article with invalid jwtauthorizaton token', async ({ loginAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token+'1111');
    const res = await articleAPI.createArticle(title, about, content);
    expect(res.status()).toEqual(401);
});

test('AF9 - Create article without jwtauthorizaton token', async ({ loginAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT('');
    const res = await articleAPI.createArticle(title, about, content);
    expect(res.status()).toEqual(401);
});

