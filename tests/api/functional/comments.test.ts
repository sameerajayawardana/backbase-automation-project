import * as faker from 'faker';
import { ArticleInfo } from '../../../apis/articleAPI';
import { user1 } from '../../../data/user';
import test, { expect } from '../../../fixtures/apis';

// Failed due to BUG-7 - Refer the test report, Bugs section
test('AF23 - Create comment without body', async ({ loginAPI, commentAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();
    const payLoad = {
        "comment": {}
    }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;

    await commentAPI.setHeaderJWT(activeUser.user.token);
    const res = await commentAPI.commentPOST(article.slug, payLoad);
    expect(res.status()).toEqual(500);
});

// Failed due to BUG-8 - Refer the test report, Bugs section
test('AF24 - Create comment with empty body', async ({ loginAPI, commentAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();
    const payLoad = {
        "comment": {
          "body": ""
        }
      }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;

    await commentAPI.setHeaderJWT(activeUser.user.token);
    const res = await commentAPI.commentPOST(article.slug, payLoad);
    expect(res.status()).toEqual(500);
});

test('AF25 - Create comment with invalid jwtauthorizaton token', async ({ loginAPI, commentAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();
    const payLoad = {
        "comment": {
          "body": await faker.lorem.sentence()
        }
      }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;

    await commentAPI.setHeaderJWT(activeUser.user.token+'111');
    const res = await commentAPI.commentPOST(article.slug, payLoad);
    expect(res.status()).toEqual(401);
});

test('AF26 - Create comment without jwtauthorizaton token', async ({ loginAPI, commentAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();
    const payLoad = {
        "comment": {
          "body": await faker.lorem.sentence()
        }
      }

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;

    await commentAPI.setHeaderJWT("");
    const res = await commentAPI.commentPOST(article.slug, payLoad);
    expect(res.status()).toEqual(401);
});