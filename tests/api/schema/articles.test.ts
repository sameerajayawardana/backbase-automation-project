import * as faker from 'faker';
import { user1 } from "../../../data/user";
import test from '../../../fixtures/apis';
import chai, { expect } from 'chai';
import { articleListSchema, articleSchema } from "../../../schema/article.schema";
import { ArticleInfo } from '../../../apis/articleAPI';
chai.use(require('chai-json-schema'));

test('AF1 - Get Article list schema validation', async ({ loginAPI, articleAPI }) => {
    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);

    const articleList = (await (await articleAPI.articleListGET()).json());
    expect(articleList).to.be.jsonSchema(articleListSchema);
});

test('AF2 - Get Article schema validation', async ({ loginAPI, articleAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;

    const articleRes = (await (await articleAPI.articleGET(article.slug)).json());
    expect(articleRes).to.be.jsonSchema(articleSchema);
});