import * as faker from 'faker';
import { user1 } from "../../../data/user";
import test from '../../../fixtures/apis';
import chai, { expect } from 'chai';
import { ArticleInfo } from '../../../apis/articleAPI';
import { commentListSchema, commentSchema } from '../../../schema/comment.schema';
chai.use(require('chai-json-schema'));

test('AF21 - Get comments list schema validation', async ({ loginAPI, articleAPI, commentAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    await commentAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;
    await commentAPI.createComment(article.slug, await faker.lorem.sentence());
    await commentAPI.createComment(article.slug, await faker.lorem.sentence());
    await commentAPI.createComment(article.slug, await faker.lorem.sentence());

    const commentList = (await (await commentAPI.commentListGET(article.slug)).json());
    expect(commentList).to.be.jsonSchema(commentListSchema);
});

// Failed due to BUG-9 - Refer the test report, Bugs section
test('AF22 - Create comment response schema validation', async ({ loginAPI, articleAPI, commentAPI }) => {
    const title = await faker.lorem.sentence();
    const about = await faker.lorem.sentence();
    const content = await faker.lorem.paragraph();

    const activeUser = await loginAPI.login(user1.email, user1.pass);
    await articleAPI.setHeaderJWT(activeUser.user.token);
    await commentAPI.setHeaderJWT(activeUser.user.token);
    const article = (await (await articleAPI.createArticle(title, about, content)).json()).article as ArticleInfo;

    const comment = (await (await commentAPI.createComment(article.slug, await faker.lorem.sentence())).json());
    expect(comment).to.be.jsonSchema(commentSchema);
});