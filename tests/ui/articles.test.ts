import * as faker from 'faker';
import test, { expect } from '../../fixtures/pages';
import { setBrowserForLogs } from '../../util/logUtil';
import { user1 } from '../../data/user';
import { ArticleAPI } from '../../apis/articleAPI';
import { searchArticleByTitle } from '../../util/articleUtil';

test('PF1 - Verify user can view articles without login', async ({ newPage, articlesPage, homePage, browserName }) => {
  setBrowserForLogs(browserName);

  await homePage.goto();
  await homePage.selectTag('matrix');
  const articles = await homePage.getArticleListItems();
  expect(articles.length).toBeGreaterThan(0);

  await homePage.readArticle(articles[0]);
  await articlesPage.followButton.click();
  expect(newPage.url()).toContain('/login');
});

test('PF2 - Verify user can create article', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();

  setBrowserForLogs(browserName);

  await homePage.goto();
  await loginPage.loginWithAPI(user1.email, user1.pass);
  await homePage.createArticleLink.click();

  await articlesPage.submitArticleForm(title, about, content, tag);
  expect(await articlesPage.commentInput.isVisible()).toEqual(true);

  await homePage.goto();
  await homePage.globalFeedLink.click();
  const articles = await homePage.getArticleListItems();
  expect(articles[0].title).toEqual(title);
  expect(articles[0].author.trim()).toEqual(user1.name);
});

test('PF3 - Verify user can update article', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();
  const updateText = '1234';

  setBrowserForLogs(browserName);

  // Precondition: Login and creating article via API
  await homePage.goto();
  const token = await loginPage.loginWithAPI(user1.email, user1.pass);
  const articleAPI = new ArticleAPI();
  await articleAPI.setHeaderJWT(token);
  await articleAPI.createArticle(title, about, content, tag);

  await homePage.goto();
  await homePage.globalFeedLink.click();
  const articles = await homePage.getArticleListItems();
  await homePage.readArticle(articles[0]);

  await articlesPage.editArticleButton.nth(0).click();
  await articlesPage.submitArticleForm(updateText, updateText, updateText, updateText);
  await articlesPage.editArticleButton.nth(0).click();
  expect(await articlesPage.titleInput.inputValue()).toEqual(updateText+title);
  expect(await articlesPage.aboutInput.inputValue()).toEqual(updateText+about);
  expect(await articlesPage.contentInput.inputValue()).toEqual(content+updateText);
});

test('PF4 - Verify user can delete article', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();

  setBrowserForLogs(browserName);

  // Precondition: Login and creating article via API
  await homePage.goto();
  const token = await loginPage.loginWithAPI(user1.email, user1.pass);
  const articleAPI = new ArticleAPI();
  await articleAPI.setHeaderJWT(token);
  await articleAPI.createArticle(title, about, content, tag);

  await homePage.goto();
  await homePage.globalFeedLink.click();
  const articles = await homePage.getArticleListItems();
  await homePage.readArticle(articles[0]);

  await articlesPage.deleteArticleButton.nth(0).click();
  await homePage.globalFeedLink.click();
  const latestArticles = await homePage.getArticleListItems();
  expect((await searchArticleByTitle(latestArticles, title)).length).toEqual(0);
});

test('PF5 - Verify article can be filtered by tags', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();

  setBrowserForLogs(browserName);

  // Precondition: Login and creating article via API
  await homePage.goto();
  const token = await loginPage.loginWithAPI(user1.email, user1.pass);
  const articleAPI = new ArticleAPI();
  await articleAPI.setHeaderJWT(token);
  await articleAPI.createArticle(title, about, content, tag);

  await homePage.goto();
  await homePage.globalFeedLink.click();
  await homePage.selectTag(tag);

  const articles = await homePage.getArticleListItems();
  expect((await searchArticleByTitle(articles, title)).length).toEqual(1);
});