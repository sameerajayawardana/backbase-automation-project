import * as faker from 'faker';
import { ArticleAPI, ArticleInfo } from '../../apis/articleAPI';
import { CommentAPI } from '../../apis/commentAPI';
import { user1, user2 } from '../../data/user';
import test, { expect } from '../../fixtures/pages';
import { searchArticleByTitle } from '../../util/articleUtil';
import { setBrowserForLogs } from '../../util/logUtil';

// Failing due to BUG-2. Refer test report Bugs section
test('PF6 - Verify logged In user can view comments in an article', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();
  const comment = await faker.lorem.sentence();

  setBrowserForLogs(browserName);

  // Precondition: Login and creating article via API
  await homePage.goto();
  const token = await loginPage.loginWithAPI(user1.email, user1.pass);
  const articleAPI = new ArticleAPI();
  await articleAPI.setHeaderJWT(token);
  const article = (await (await articleAPI.createArticle(title, about, content, tag)).json()).article as ArticleInfo;
  const commentAPI = new CommentAPI();
  await commentAPI.setHeaderJWT(token);
  await commentAPI.createComment(article.slug, comment);

  await loginPage.loginWithAPI(user2.email, user2.pass);
  await homePage.goto();
  await homePage.globalFeedLink.click();
  let articles = await homePage.getArticleListItems();
  await homePage.readArticle((await searchArticleByTitle(articles, title))[0]);
  expect(await articlesPage.commentItem.isVisible()).toEqual(true);
  expect(await articlesPage.getCommentText()).toEqual(comment);
});

test('PF7 - Verify logged In user can add comments to an article', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();
  const comment = await faker.lorem.sentence();

  setBrowserForLogs(browserName);

  // Precondition: Login and creating article via API
  await homePage.goto();
  const token = await loginPage.loginWithAPI(user1.email, user1.pass);
  const articleAPI = new ArticleAPI();
  await articleAPI.setHeaderJWT(token);
  await articleAPI.createArticle(title, about, content, tag);

  await loginPage.loginWithAPI(user2.email, user2.pass);

  await homePage.goto();
  await homePage.globalFeedLink.click();
  let articles = await homePage.getArticleListItems();
  await homePage.readArticle((await searchArticleByTitle(articles, title))[0]);

  await articlesPage.addComment(comment);
  expect(await articlesPage.commentItem.isVisible()).toEqual(true);
  expect(await articlesPage.getCommentText()).toEqual(comment);
});


// Failing due to BUG-2. Refer test report Bugs section
test('PF8 - Verify logged In user can delete comments to an article', async ({ loginPage, articlesPage, homePage, browserName }) => {
  const title = await faker.lorem.sentence();
  const about = await faker.lorem.sentence();
  const content = await faker.lorem.paragraph();
  const tag = await faker.lorem.word();
  const comment = await faker.lorem.sentence();

  setBrowserForLogs(browserName);

  // Precondition: Login and creating article via API
  await homePage.goto();
  const token = await loginPage.loginWithAPI(user1.email, user1.pass);
  const articleAPI = new ArticleAPI();
  await articleAPI.setHeaderJWT(token);
  const article = (await (await articleAPI.createArticle(title, about, content, tag)).json()).article as ArticleInfo;
  const commentAPI = new CommentAPI();
  await commentAPI.setHeaderJWT(token);
  await commentAPI.createComment(article.slug, comment);

  await loginPage.loginWithAPI(user2.email, user2.pass);
  await homePage.goto();
  await homePage.globalFeedLink.click();
  let articles = await homePage.getArticleListItems();
  await homePage.readArticle((await searchArticleByTitle(articles, title))[0]);

  await articlesPage.deleteCommentButton.click();
  expect(await articlesPage.commentItem.isVisible()).toEqual(false);
  expect(await articlesPage.deleteCommentButton.isVisible()).toEqual(false);
});