export const LOGIN = "/users/login";
export const ARTICLES = "/articles";
export const COMMENTS = "/articles/:slug/comments";
