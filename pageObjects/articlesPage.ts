import { expect, Locator, Page } from '@playwright/test';
import { delay, log } from '../util/logUtil';

export class ArticlesPage {
  readonly page: Page;
  readonly followButton: Locator;
  readonly titleInput: Locator;
  readonly aboutInput: Locator;
  readonly contentInput: Locator;
  readonly tagsInput: Locator;
  readonly publishArticleButton: Locator;
  readonly commentInput: Locator;
  readonly editArticleButton: Locator;
  readonly deleteArticleButton: Locator;
  readonly postCommentButton: Locator;
  readonly commentItem: Locator;
  readonly deleteCommentButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.followButton = page.locator('div[class="banner"] app-follow-button');
    this.titleInput = page.locator('input[formcontrolname="title"]');
    this.aboutInput = page.locator('input[formcontrolname="description"]');
    this.contentInput = page.locator('textarea[formcontrolname="body"]');
    this.tagsInput = page.locator('input[placeholder="Enter tags"]');
    this.publishArticleButton = page.locator('div[class="editor-page"] button');
    this.commentInput = page.locator('textarea[placeholder="Write a comment..."]');
    this.editArticleButton = page.locator('div[class="banner"] i[class="ion-edit"]');
    this.deleteArticleButton = page.locator('div[class="banner"] i[class="ion-trash-a"]');
    this.postCommentButton = page.locator('//button[text()=" Post Comment "]');
    this.commentItem = page.locator('app-article-comment p');
    this.deleteCommentButton = page.locator('div[class="card-footer"] i[class="ion-trash-a"]');
  }

  /**
   * This method will fill and submit create article form
   *
   * @return {*}  {Promise<void>}
   * @memberof ArticlesPage
   */
  async submitArticleForm(title: string, about: string, body: string, tags?: string) : Promise<void> {
    log(`Submit article form: ${title}`)
    await this.titleInput.type(title);
    await this.aboutInput.type(about);
    await this.contentInput.type(body);
    if(tags)
      await this.tagsInput.type(tags);
    await this.publishArticleButton.click();
    await delay(2000);
  }

  async addComment(comment: string) : Promise<void> {
    log('Adding comment to article')
    await this.commentInput.type(comment);
    await this.postCommentButton.click();
    await delay(2000);
  }

  async getCommentText() : Promise<string> {
    return (await this.commentItem.textContent()).trim();
  }
}