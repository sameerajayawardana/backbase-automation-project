import { Locator, Page } from '@playwright/test';
import { Article, extractAllArticles } from '../util/articleUtil';
import { delay, log } from '../util/logUtil';

export class HomePage {
  readonly page: Page;
  readonly createArticleLink: Locator;
  readonly globalFeedLink: Locator;
  readonly articleList: Locator;
  readonly articleItemSelector = 'div[class="article-preview"]';

  constructor(page: Page) {
    this.page = page;
    this.createArticleLink = page.locator('a[href="/editor"]');
    this.globalFeedLink = page.locator('//a[text()=" Global Feed "]');
    this.articleList = page.locator('app-article-list');
  }

  async goto() : Promise<void> {
    log('Navigating to home');
    await this.page.goto('/');
  }

  async reloadPage() : Promise<void> {
    await this.page.reload();
  }

  async selectTag(tagName: string): Promise<void> {
    await this.page.click(`//a[text() = " ${tagName} " ]`);
    await delay(2000);
  }

  async getArticleListItems() : Promise<Article[]>{
    await delay(2000);
    return extractAllArticles(this.page, this.articleItemSelector);
  }

  async readArticle(article: Article) : Promise<void> {
    await article.viewLink.click();
    await delay(2000);
  }
}