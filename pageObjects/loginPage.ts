import { Locator, Page } from '@playwright/test';
import { LoginAPI } from '../apis/loginAPI';
import { log } from '../util/logUtil';

export class LoginPage {
  readonly page: Page;
  readonly loginLink: Locator;

  constructor(page: Page) {
    this.page = page;
    this.loginLink = page.locator('a[href="/login"]');
  }

  /**
   * This method will navigate browser to login page
   *
   * @return {*}  {Promise<void>}
   * @memberof LoginPage
   */
  async goto(): Promise<void> {
    log('Navigating to home page');
    await this.loginLink.click();
  }

  /**
   * This method will set jwt in localstorage
   *
   * @return {*}  {Promise<void>}
   * @memberof LoginPage
   */
  async loginWithAPI(email: string, password: string): Promise<string> {
    const loginAPI = new LoginAPI();
    const res = await loginAPI.login(email, password);
    await loginAPI.setHeaderJWT(res.user.token);

    await this.page.evaluate(
      `window.localStorage.setItem('jwtToken', '${res.user.token}')`
    );

    await this.page.reload();
    log(`Logged in with "${email}"`);
    return res.user.token;
  }
}