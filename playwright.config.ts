import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  use: {
    baseURL: (process.env.TEST_ENVIRONMENT=="prod"? process.env.PROD_BASE_URL : process.env.QA_BASE_URL) || "https://qa-task.backbasecloud.com",
    headless: true,
    screenshot: "only-on-failure",
  },
  reporter: [ ['list'], ['allure-playwright'] ],

  projects: [
    {
      name: 'Chromium',
      use: {
        browserName: 'chromium',
      },
    },
    // {
    //   name: 'Firefox',
    //   use: { browserName: 'firefox' },
    // },
  ],

  workers: 3,
  retries: 1
};
export default config;