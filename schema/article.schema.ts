export const articleListSchema = {
    "$schema": "/api/articles",
    "type": "object",
    "properties": {
      "articles": {
        "type": "array",
        "items": [
          {
            "type": "object",
            "properties": {
              "slug": {
                "type": "string"
              },
              "title": {
                "type": "string"
              },
              "description": {
                "type": "string"
              },
              "body": {
                "type": "string"
              },
              "tagList": {
                "type": "array",
                "items": [
                  {
                    "type": "string"
                  },
                  {
                    "type": "string"
                  }
                ]
              },
              "createdAt": {
                "type": "string"
              },
              "updatedAt": {
                "type": "string"
              },
              "favorited": {
                "type": "boolean"
              },
              "favoritesCount": {
                "type": "integer"
              },
              "author": {
                "type": "object",
                "properties": {
                  "username": {
                    "type": "string"
                  },
                  "bio": {
                    "type": "string"
                  },
                  "image": {
                    "type": "string"
                  },
                  "following": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "username",
                  "image",
                  "following"
                ]
              }
            },
            "required": [
              "slug",
              "title",
              "description",
              "body",
              "createdAt",
              "updatedAt",
              "favorited",
              "favoritesCount",
              "author"
            ]
          }
        ]
      },
      "articlesCount": {
        "type": "integer"
      }
    },
    "required": [
      "articles",
      "articlesCount"
    ]
  }

export const articleSchema = {
    "$schema": "/api/articles/:slug",
    "type": "object",
    "properties": {
      "article": {
        "type": "object",
        "properties": {
          "slug": {
            "type": "string"
          },
          "title": {
            "type": "string"
          },
          "description": {
            "type": "string"
          },
          "body": {
            "type": "string"
          },
          "tagList": {
            "type": "array",
            "items": [
              {
                "type": "string"
              },
              {
                "type": "string"
              }
            ]
          },
          "createdAt": {
            "type": "string"
          },
          "updatedAt": {
            "type": "string"
          },
          "favorited": {
            "type": "boolean"
          },
          "favoritesCount": {
            "type": "integer"
          },
          "author": {
            "type": "object",
            "properties": {
              "username": {
                "type": "string"
              },
              "bio": {
                "type": "string"
              },
              "image": {
                "type": "string"
              },
              "following": {
                "type": "boolean"
              }
            },
            "required": [
              "username",
              "image",
              "following"
            ]
          }
        },
        "required": [
          "slug",
          "title",
          "description",
          "body",
          "createdAt",
          "updatedAt",
          "favorited",
          "favoritesCount",
          "author"
        ]
      }
    },
    "required": [
      "article"
    ]
  }