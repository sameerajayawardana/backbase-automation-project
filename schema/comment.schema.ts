export const commentListSchema = {
    "$schema": "GET /api/articles/:slug/comments",
    "type": "object",
    "properties": {
      "comments": {
        "type": "array",
        "items": [
          {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer"
              },
              "createdAt": {
                "type": "string"
              },
              "updatedAt": {
                "type": "string"
              },
              "body": {
                "type": "string"
              },
              "author": {
                "type": "object",
                "properties": {
                  "username": {
                    "type": "string"
                  },
                  "bio": {
                    "type": "string"
                  },
                  "image": {
                    "type": "string"
                  },
                  "following": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "username",
                  "bio",
                  "image",
                  "following"
                ]
              }
            },
            "required": [
              "id",
              "createdAt",
              "updatedAt",
              "body",
              "author"
            ]
          }
        ]
      }
    },
    "required": [
      "comments"
    ]
  };

export const commentSchema = {
    "$schema": "POST /api/articles/:slug/comments",
    "type": "object",
    "properties": {
      "comment": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer"
          },
          "createdAt": {
            "type": "string"
          },
          "updatedAt": {
            "type": "string"
          },
          "body": {
            "type": "string"
          },
          "author": {
            "type": "object",
            "properties": {
              "username": {
                "type": "string"
              },
              "bio": {
                "type": "string"
              },
              "image": {
                "type": "string"
              },
              "following": {
                "type": "boolean"
              }
            },
            "required": [
              "username",
              "bio",
              "image",
              "following"
            ]
          }
        },
        "required": [
          "id",
          "createdAt",
          "body",
          "author"
        ]
      }
    },
    "required": [
      "comment"
    ]
  };